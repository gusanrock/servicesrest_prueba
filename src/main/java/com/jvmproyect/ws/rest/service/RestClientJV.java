package com.jvmproyect.ws.rest.service;

import com.jvmproyect.ws.rest.vo.VOUser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class RestClientJV {
 
	/**
	 * @param args
	 */
	public static void main(String[]args) {
	
	String urlRestService = "http://localhost:8080/RestJV/services/JavaService/ValidaUsuario";
	System.out.println("####### Invoke Rest Service: ["+urlRestService+"] ");	
		
	VOUser vo = new VOUser();
	vo.setUsuarios("JV");
	vo.setPasswords("JavaService");
	vo.setUserValido(false);
	
	//Vamos a crear un objeto (ClientConfig) 
	
	ClientConfig clientConfig = new DefaultClientConfig();
	clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,Boolean.TRUE);
	Client client = Client.create(clientConfig);
	WebResource webResource = client.resource(urlRestService);
	ClientResponse response = webResource.type("application/json").post(ClientResponse.class, vo);
	vo = response.getEntity(VOUser.class);
	System.out.println("####### Response: [Usuario: "+vo.getUsuarios()+"]");
	System.out.println("####### Response: [User is valid: "+vo.isUserValido()+"]");
}
}