package com.jvmproyect.ws.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.jvmproyect.ws.rest.vo.VOUser;

@Path("/JavaService")

public class ServiceLoginJV {
	
	//Declarar que metodo vamos a usar
	//Vamos a invocar el servicio ValidaUsuario por POST
	@POST
	@Path("/ValidaUsuario")
	//Anotacion(Consumes)Se refiere a que va a consumir en este caso Json
	@Consumes({MediaType.APPLICATION_JSON})
	//Anotacion(Produces)Que es lo que va regresar nuestro metodo en este caso va devolver un objeto VOUser Todo lo que esta adentro lo va transformar en obj Json
	@Produces({MediaType.APPLICATION_JSON})
	public VOUser validaUsuario(VOUser vo) {
	    vo.setUserValido(false);
		if(vo.getUsuarios().equals("JV")&& vo.getPasswords().equals("JavaService")) {
         vo.setUserValido(true);
}
		vo.setPasswords("##############");
	return vo;
}
}